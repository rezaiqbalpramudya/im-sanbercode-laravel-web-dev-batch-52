<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Contoh Array</h3>
    <?php
    echo "<h4>Soal 1 Array</h4>";

    $trainer = ["Reza", "Iqbal", "Pramudya", "Namanya"];

    print_r($trainer);


    echo "<h4>Soal 2 Array</h4>";

    echo "Total Trainer : " . count($trainer);
    echo "<ol>";
    echo "<li>" . $trainer[0] . "</li>";
    echo "<li>" . $trainer[1] . "</li>";
    echo "<li>" . $trainer[2] . "</li>";
    echo "<li>" . $trainer[3] . "</li>";
    echo "</ol>";


    echo "<h4>Soal 3 Array</h4>";

    $biodataTrainer = [
        ["id" => 1, "nama" => "Reza", "materi" => "Laravel"],
        ["id" => 2, "nama" => "Iqbal", "materi" => "ReactJs"],
        ["id" => 3, "nama" => "Pramudya", "materi" => "Python"],
        ["id" => 4, "nama" => "Namanya", "materi" => "DM"],
    ];

    echo "<pre>";
    print_r($biodataTrainer);
    echo "</pre>";

    ?>
</body>
</html>