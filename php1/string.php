<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Hello World</h1>

    <?php
    echo "<h4>Contoh 1</h4>";

    $kalimat1 = "Selamat Datang di Sanbercode";
    echo "Kalimat 1 :  . $kalimat1 <br>";
    echo "Panjang kalimat 1 : ". strlen($kalimat1) . "<br>";
    echo "Jumlah Kata Kalimat 1 : " . str_word_count($kalimat1) . "<br>";

    echo "<h4>Contoh 2</h4>";

    $kalimat2 = "Nama saya Reza";
    echo "Kata 1 Kalimat 2 : " . substr($kalimat2,0,4) . "<br>";
    echo "Kata 2 Kalimat 2 : " . substr($kalimat2,5,4) . "<br>";
    echo "Kata 3 Kalimat 2 : " . substr($kalimat2,10,4) . "<br>";

    echo "<h4>COntoh 3</h4>";

    $kalimat3 = "Selamat Pagi";
    echo "Kalimat 3 : " . $kalimat3 . "<br>";
    echo "Kalimat 3 ubah : " . str_replace("Pagi", "Malam", $kalimat3) . "<br>";

    ?>
</body>
</html>